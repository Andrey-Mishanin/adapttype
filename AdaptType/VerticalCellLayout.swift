//
//  VerticalCellLayout.swift
//  AdaptType
//
//  Created by Andrey Mishanin on 08/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import UIKit

struct VerticalCellLayout: CellLayout {
  private let viewModel: CellViewModel
  private let width: CGFloat

  init(viewModel: CellViewModel, width: CGFloat) {
    self.viewModel = viewModel
    self.width = width
  }

  var textFrame: CGRect {
    return CGRect(origin: CGPoint(x: contentInset.left, y: contentInset.top),
                  size: CGSize(width: maxTextWidth, height: textSize.height))
  }

  private var textSize: CGSize {
    let boundingRect = viewModel.text.boundingRect(with: CGSize(width: maxTextWidth, height: .greatestFiniteMagnitude),
                                                   options: .usesLineFragmentOrigin,
                                                   context: nil)
    return boundingRect.integral.size
  }

  private var maxTextWidth: CGFloat {
    return width - (contentInset.left + contentInset.right)
  }

  var switchFrame: CGRect {
    return CGRect(origin: CGPoint(x: contentInset.left, y: textFrame.maxY + gapBetweenTextAndSwitch),
                  size: switchSize)
  }

  private let switchSize = CGSize(width: 51, height: 31)
  private let gapBetweenTextAndSwitch = 3 as CGFloat

  let contentInset = UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 12)
}
