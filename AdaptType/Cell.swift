//
//  Cell.swift
//  AdaptType
//
//  Created by Andrey Mishanin on 08/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import UIKit

final class Cell: UITableViewCell {
  static let reuseID = "Cell"

  var viewModel: CellViewModel! {
    didSet {
      textLabel?.attributedText = viewModel.text
      setNeedsLayout()
    }
  }

  private let aSwitch = UISwitch(frame: .zero)

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    textLabel?.numberOfLines = 0
    textLabel?.adjustsFontForContentSizeCategory = false
    aSwitch.isOn = true
    contentView.addSubview(aSwitch)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func layoutSubviews() {
    super.layoutSubviews()

    let layout = viewModel.layoutFor(width: contentView.safeBounds.width)
    textLabel?.frame = layout.textFrame
    aSwitch.frame = layout.switchFrame
  }
}
