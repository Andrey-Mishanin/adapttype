//
//  Extensions.swift
//  AdaptType
//
//  Created by Andrey Mishanin on 08/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import UIKit

extension String {
  func withAttributes(_ attrs: [NSAttributedString.Key : Any]) -> NSAttributedString {
    return NSAttributedString(string: self, attributes: attrs)
  }
}

extension UIView {
  var safeBounds: CGRect {
    return bounds.inset(by: safeAreaInsets)
  }
}
