//
//  CellLayout.swift
//  AdaptType
//
//  Created by Andrey Mishanin on 08/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import UIKit

protocol CellLayout {
  init(viewModel: CellViewModel, width: CGFloat)
  var textFrame: CGRect { get }
  var switchFrame: CGRect { get }
  var height: CGFloat { get }
  var contentInset: UIEdgeInsets { get }
}

extension CellLayout {
  var height: CGFloat {
    return [textFrame.maxY, switchFrame.maxY].max()! + contentInset.bottom
  }
}
