//
//  ViewController.swift
//  AdaptType
//
//  Created by Andrey Mishanin on 18/10/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import UIKit

final class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  private let tableView = UITableView(frame: .zero, style: .grouped)

  override func viewDidLoad() {
    tableView.register(Cell.self, forCellReuseIdentifier: Cell.reuseID)
    tableView.dataSource = self
    tableView.delegate = self
    view.addSubview(tableView)
  }

  override func viewWillLayoutSubviews() {
    tableView.frame = view.bounds
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: Cell.reuseID, for: indexPath) as! Cell
    cell.viewModel = viewModel
    return cell
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return viewModel.layoutFor(width: tableView.safeBounds.width).height
  }

  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    super.traitCollectionDidChange(previousTraitCollection)
    guard previousTraitCollection?.preferredContentSizeCategory != traitCollection.preferredContentSizeCategory else {
      return
    }
    let contentSizeCategory = traitCollection.preferredContentSizeCategory
    viewModel = CellViewModel(
      text: "Larger Accessibility Sizes".withAttributes([.font : UIFont.preferredFont(forTextStyle: .body)]),
      layoutDirection: contentSizeCategory.isAccessibilityCategory ? .vertical : .horizontal
    )
  }

  private var viewModel: CellViewModel! {
    didSet {
      tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
    }
  }
}
