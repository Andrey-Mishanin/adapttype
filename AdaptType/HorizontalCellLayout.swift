//
//  HorizontalCellLayout.swift
//  AdaptType
//
//  Created by Andrey Mishanin on 08/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import UIKit

struct HorizontalCellLayout: CellLayout {
  private let viewModel: CellViewModel
  private let width: CGFloat

  init(viewModel: CellViewModel, width: CGFloat) {
    self.viewModel = viewModel
    self.width = width
  }

  var textFrame: CGRect {
    return CGRect(origin: CGPoint(x: contentInset.left, y: textOriginY),
                  size: CGSize(width: maxTextWidth, height: textSize.height))
  }

  private var textOriginY: CGFloat {
    if textSize.height > switchSize.height {
      return contentInset.top
    } else {
      return switchOriginY + (switchSize.height - textSize.height) / 2
    }
  }

  private var textSize: CGSize {
    let boundingRect = viewModel.text.boundingRect(with: CGSize(width: maxTextWidth, height: .greatestFiniteMagnitude),
                                                   options: .usesLineFragmentOrigin,
                                                   context: nil)
    return boundingRect.integral.size
  }

  private var maxTextWidth: CGFloat {
    return switchOriginX - contentInset.left - gapBetweenTextAndSwitch
  }

  var switchFrame: CGRect {
    return CGRect(origin: CGPoint(x: switchOriginX, y: switchOriginY),
                  size: switchSize)
  }

  private var switchOriginX: CGFloat {
    return width - contentInset.right - switchSize.width
  }

  private var switchOriginY: CGFloat {
    if textSize.height > switchSize.height {
      return textFrame.minY + (textFrame.height - switchSize.height) / 2
    } else {
      return contentInset.top
    }
  }

  private let switchSize = CGSize(width: 51, height: 31)
  private let gapBetweenTextAndSwitch = 3 as CGFloat

  let contentInset = UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 12)
}
