//
//  CellViewModel.swift
//  AdaptType
//
//  Created by Andrey Mishanin on 08/11/2018.
//  Copyright © 2018 Andrey Mishanin. All rights reserved.
//

import Foundation
import CoreGraphics

struct CellViewModel {
  enum LayoutDirection {
    case horizontal
    case vertical
  }

  let text: NSAttributedString
  let layoutDirection: LayoutDirection
}

extension CellViewModel {
  func layoutFor(width: CGFloat) -> CellLayout {
    switch layoutDirection {
    case .horizontal:
      return HorizontalCellLayout(viewModel: self, width: width)
    case .vertical:
      return VerticalCellLayout(viewModel: self, width: width)
    }
  }
}
